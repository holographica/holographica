using System;
using System.Linq;
using DotNetTools.SharpGrabber;
using DotNetTools.SharpGrabber.Grabbed;
using UnityEngine;

public class Grabber : MonoBehaviour
{
	private async void Start()
	{
		var grabber = GrabberBuilder.New()
			.UseDefaultServices()
			.AddYouTube()
			.Build();
		
		var result = await grabber.GrabAsync(new Uri("https://www.youtube.com/watch?v=LTseTg48568"));
		var info = result.Resource<GrabbedInfo>();
		print($"Time Length: {info.Length}");
		var images = result.Resources<GrabbedImage>();
		print($"Got {images.Count()} images.");
		var videos = result.Resources<GrabbedMedia>();
		var sortedMediaFiles = videos.OrderByResolutionDescending().ThenByBitRateDescending().ToArray();
		var bestVideo = videos.GetHighestQualityVideo();
		var bestAudio = videos.GetHighestQualityAudio();
		print(bestVideo.OriginalUri.AbsoluteUri);
	}
}