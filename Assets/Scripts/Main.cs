﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DG.Tweening;
using HtmlAgilityPack;
using LiteDB;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

#pragma warning disable CS4014

public class Main : MonoBehaviour
{
	public static ILiteCollection<Post> Collection;
	public static ILiteStorage<string> Storage;

	public RectTransform postsParent;

	private static LiteDatabase _db;
	private readonly Placeholder[] _placeholders = new Placeholder[10];
	private static List<Post> _currentPosts = new List<Post>();
	private int _currentPage = 1;
	private FullPost _fullPost;

	private const string WPPage = "https://holographica.space/wp-json/wp/v2/posts?page=";
	private const string WPTag = "https://holographica.space/wp-json/wp/v2/posts?tag=";
	private const string WPId = "https://holographica.space/wp-json/wp/v2/posts?include[]=";
	private const string Limits = "&_fields=id,title,excerpt,content,link,date_gmt";

	private CancellationTokenSource _token;

	private void Awake()
	{
		_db = new LiteDatabase(Path.Combine(Application.persistentDataPath, "holo.db"));
		_fullPost = FindObjectOfType<FullPost>();
		_token = new CancellationTokenSource();
		Collection = _db.GetCollection<Post>("Posts");
		Storage = _db.FileStorage;
	}

	private async void Start()
	{
		InitPlaceholders();

		ReadLocalWP();
		await ReadRemoteWP(_currentPage);

		void InitPlaceholders()
		{
			for (var i = 0; i < postsParent.childCount - 1; i++)
			{
				var go = postsParent.GetChild(i).gameObject;

				var rootRect = go.GetComponent<RectTransform>();
				var contentRect = rootRect.Find("Content").GetComponent<RectTransform>();
				var thumbnail = go.GetComponentInChildren<RawImage>();
				var title = contentRect.Find("Title");
				var titleText = title.GetComponent<TMP_Text>();
				var excerpt = contentRect.Find("Excerpt");
				var excerptText = excerpt.GetComponent<TMP_Text>();
				var readButton = contentRect.GetComponentInChildren<Button>();

				var placeHolder = new Placeholder
					{
						thumbnail = thumbnail, title = titleText, excerpt = excerptText, readButton = readButton
					};

				_placeholders[i] = placeHolder;
			}
		}
	}

	private void OnDestroy()
	{
		_token.Cancel();
	}

	private void ReadLocalWP()
	{
		_currentPosts = Collection.Query().OrderByDescending(post => post.Date_GMT).Limit(10).ToList();
		if (_currentPosts.Count == 0) return;
		PopulatePlaceholders();
	}

	private async Task ReadRemoteWP(int page)
	{
		var remotePosts = await GetByPage();
		if (remotePosts == null) return;

		if (CheckForNewPosts(out var posts))
		{
			for (var i = 0; i < posts.Count; i++)
			{
				var newPost = posts[i];
				Collection.Insert(newPost);
				DownloadMedia(newPost);
			}

			_currentPosts = Collection.Query().OrderByDescending(post => post.Date_GMT).Limit(10).ToList();

			PopulatePlaceholders();
		}

		bool CheckForNewPosts(out List<Post> newPosts)
		{
			if (Collection.Count() > 0)
			{
				newPosts = remotePosts.Where(remotePost => Collection.Query().Where(post => post.Id == remotePost.Id).Count() == 0).ToList();
				return newPosts.Count > 0;
			}

			newPosts = remotePosts;
			return true;
		}

		async Task<List<Post>> GetByPage()
		{
			var www = new UnityWebRequest { method = UnityWebRequest.kHttpVerbGET, url = WPPage + page + Limits };
			var dhJson = new DownloadHandlerBuffer();
			www.downloadHandler = dhJson;
			www.SetRequestHeader("Content-Type", "application/json");
			var task = www.SendWebRequest();
			while (!task.isDone) await Task.Yield();
			if (task.webRequest.result != UnityWebRequest.Result.ProtocolError && task.webRequest.result != UnityWebRequest.Result.ConnectionError)
				return JsonConvert.DeserializeObject<List<Post>>(dhJson.text);
			print("Request failed.");
			return null;
		}

		void DownloadMedia(Post post)
		{
			var content = post.Content.Rendered;
			var html = new HtmlDocument();
			html.LoadHtml(content);

			var nodes = html.DocumentNode.SelectNodes("//img");
			if (nodes == null) return; //todo check for errors

			for (var i = 0; i < nodes.Count; i++)
			{
				var node = nodes[i];
				if (!node.HasAttributes) continue;

				var attributes = node.Attributes.AttributesWithName("src").ToList();

				for (var j = 0; j < attributes.Count; j++)
				{
					var index = i;

					var attribute = attributes[j];
					var isGif = attribute.Value.Split('.').Last() == "gif";
					var fileId = isGif ? $"{post.Id}/{index}.gif" : $"{post.Id}/{index}";
					content = content.Replace(attribute.Value, fileId);

					// print($"Downloading {attribute.Value} at index {i}");

					Download(attribute.Value).ContinueWith(t => {
						Storage.Upload(fileId, fileId.Split('/').Last(), t.Result);
					});
				}
			}

			post.Content = new Content { Rendered = content };
			Collection.Update(post);

			async Task<MemoryStream> Download(string link)
			{
				//todo check for errors
				var requestImage = new UnityWebRequest(link);
				var dhImage = new DownloadHandlerBuffer();
				requestImage.downloadHandler = dhImage;
				var t = requestImage.SendWebRequest();
				while (!t.isDone) await Task.Yield();
				var result = t.webRequest.result;
				if (result == UnityWebRequest.Result.ConnectionError || result == UnityWebRequest.Result.ProtocolError || result == UnityWebRequest.Result.DataProcessingError)
				{
					print("error");
					return null;
				}

				return new MemoryStream(dhImage.data);
			}
		}
	}

	private void PopulatePlaceholders()
	{
		postsParent.anchoredPosition = Vector2.zero;

		for (var i = 0; i < 10; i++)
		{
			var post = _currentPosts[i];
			var ph = _placeholders[i];
			ph.title.text = post.Title.Rendered;
			ph.excerpt.text = post.Excerpt.Rendered.Replace("<p>", string.Empty).Replace("</p>", string.Empty);
			ph.readButton.onClick.RemoveAllListeners();
			ph.readButton.onClick.AddListener(() => _fullPost.OpenPost(GetByID(post.Id).Result));

			GetThumbnail(post, ph.thumbnail);
		}

		async Task GetThumbnail(Post post, RawImage image)
		{
			while (Storage.FindById($"{post.Id}/0") == null) await Task.Yield();

			if (_token.IsCancellationRequested) return;

			var tex = new Texture2D(1, 1);
			var stream = new MemoryStream();
			Storage.Download($"{post.Id}/0", stream);

			try
			{
				var bytes = stream.GetBuffer();
				tex.LoadImage(bytes);
				image.texture = tex;
			}
			catch (Exception e)
			{
				print(e);
			}
		}
	}

	private static async Task<Post> GetByID(int id)
	{
		var posts = Collection.Query().Where(post => post.Id == id);
		if (posts.Count() > 0) return posts.First();

		var www = new UnityWebRequest { method = UnityWebRequest.kHttpVerbGET, url = WPId + id + Limits };
		var dhJson = new DownloadHandlerBuffer();
		www.downloadHandler = dhJson;
		www.SetRequestHeader("Content-Type", "application/json");
		var task = www.SendWebRequest();
		while (!task.isDone) await Task.Yield();
		if (task.webRequest.result != UnityWebRequest.Result.ProtocolError && task.webRequest.result != UnityWebRequest.Result.ConnectionError) return JsonConvert.DeserializeObject<Post>(dhJson.text);
		print("Request failed.");
		return null;
	}

	public async void NextPage()
	{
		// if (_currentPage == _localPosts.Count / 10) return;
		await ReadRemoteWP(++_currentPage);
		postsParent.DOAnchorPos(Vector2.zero, 0.5f);
	}

	public async void PrevPage()
	{
		if (_currentPage == 1) return;
		await ReadRemoteWP(--_currentPage);
		postsParent.DOAnchorPos(Vector2.zero, 0.5f);
	}
}