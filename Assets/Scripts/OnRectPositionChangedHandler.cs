using UnityEngine;

public class OnRectPositionChangedHandler : MonoBehaviour
{
    public delegate void OnRectPositionChangedDelegate(float deltaPosition);

    public event OnRectPositionChangedDelegate OnRectPositionChanged;

    private RectTransform _contentRect;

    private void Awake()
    {
        _contentRect = transform.parent.GetComponent<RectTransform>();
    }

    private void LateUpdate()
    {
        if (_contentRect.hasChanged) OnRectPositionChanged?.Invoke(_contentRect.anchoredPosition.y);
    }
}
