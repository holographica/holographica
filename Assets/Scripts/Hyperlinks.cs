using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(TextMeshProUGUI))]
public class Hyperlinks : MonoBehaviour, IPointerClickHandler
{
    private TMP_Text _text;

    private void Awake()
    {
        _text = GetComponent<TMP_Text>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        var linkIndex = TMP_TextUtilities.FindIntersectingLink(_text, Input.mousePosition, null);
        if (linkIndex == -1) return;

        // was a link clicked?
        var linkInfo = _text.textInfo.linkInfo[linkIndex];
        print(linkInfo.GetLinkID());
        print(linkInfo.GetLinkText());

        // open the link id as a url, which is the metadata we added in the text field
        Application.OpenURL(linkInfo.GetLinkID());
    }
}