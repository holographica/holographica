using UnityEngine;

public class HeaderSafeArea : MonoBehaviour
{
	private RectTransform _header;

	private void Awake()
	{
		_header = GetComponent<RectTransform>();
	}

	private void Start()
	{
		var oldSize = _header.sizeDelta;
		var sa = Screen.safeArea;
		var y = Screen.height - sa.yMax;
		var scale = Mathf.Round(Screen.width / 375f);
		y = Mathf.Ceil(y / scale);
		_header.sizeDelta = new Vector2(oldSize.x, oldSize.y + y);
	}
}