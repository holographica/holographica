using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class Placeholder
{
    public RawImage thumbnail;
    public TMP_Text title;
    public TMP_Text excerpt;
    public Button readButton;
}