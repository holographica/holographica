﻿public class Post
{
    public Title Title { get; set; }
    public Excerpt Excerpt { get; set; }
    public Content Content { get; set; }
    public string Link { get; set; }
    public int Id { get; set; }
    public string Date_GMT { get; set; }
}

public class Title
{
    public string Rendered { get; set; }
}

public class Excerpt
{
    public string Rendered { get; set; }
}

public class Content
{
    public string Rendered { get; set; }
}
