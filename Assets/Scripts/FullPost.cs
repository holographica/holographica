using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DG.Tweening;
using HtmlAgilityPack;
using ThreeDISevenZeroR.UnityGifDecoder;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;

#pragma warning disable CS4014

public class FullPost : MonoBehaviour
{
    public Canvas fullPost;
    public Button shareButton;
    public RawImage featuredImage;
    public TMP_Text title;
    public RectTransform contentParent;
    public GameObject imagePrefab;
    public GameObject textPrefab;
    public GameObject videoPrefab;

    private RectTransform _postRect;
    private readonly List<Gif> _gifs = new List<Gif>();
    private float _gifCounter;
    private CancellationTokenSource _token;
    private const float Duration = 0.3f;

    private void Awake()
    {
        _postRect = fullPost.GetComponent<RectTransform>();
    }

    public async void OpenPost(Post post)
    {
        _token = new CancellationTokenSource();
        
        fullPost.enabled = true;
        title.text = post.Title.Rendered;
        var tween = _postRect.DOAnchorPosX(0, Duration).SetEase(Ease.Linear);
        while (!tween.IsActive()) await Task.Yield();

        var html = new HtmlDocument();
        html.LoadHtml(post.Content.Rendered);
        var paragraphs = html.DocumentNode.SelectNodes("//p");
        
        var thumbnailFound = false;
        
        for (var i = 0; i < paragraphs.Count; i++)
        {
            var isImage = false;
            var isVideo = false;

            var paragraph = paragraphs[i];
            var childNodes = paragraph.ChildNodes;

            // if the paragraph is an image
            if (ShowImage()) continue;

            // if the paragraph is a video
            if (IsVideo()) continue;

            // if it's a text
            ShowText();

            // change html to rich text
            static void ReplaceHrefWithLink(HtmlNode node)
            {
                for (var i = node.Attributes.Count - 1; i >= 0; i--)
                {
                    var attribute = node.Attributes[i];
                    if (attribute.Name == "href") attribute.Name = "link";
                    else attribute.Remove();
                }
            }

            bool ShowImage()
            {
                for (var o = 0; o < childNodes.Count; o++)
                {
                    var child = childNodes[o];
                    if (child.Name != "img") continue;
                    isImage = true;

                    var attributes = child.Attributes;
                    for (var j = 0; j < attributes.Count; j++)
                    {
                        var attribute = attributes[j];
                        if (attribute.Name != "src") continue;
                        
                        var stream = new MemoryStream();
                        Main.Storage.Download(attribute.Value, stream);
                        var bytes = stream.GetBuffer();

                        if (attribute.Value.Split('.').Last().Contains("gif"))
                        {
                            ShowGif(bytes);
                            continue;
                        }

                        var tex = new Texture2D(1, 1);
                        tex.LoadImage(bytes);
                        
                        if (!thumbnailFound)
                        {
                            thumbnailFound = true;
                            featuredImage.texture = tex;
                        }
                        else
                        {
                            var pic = Instantiate(imagePrefab, contentParent).GetComponent<RawImage>();
                            pic.texture = tex;
                        }
                    }
                }
                
                return isImage;

                void ShowGif(byte[] bytes)
                {
                    var frames = new List<Texture>();
                    var frameDelays = new List<float>();

                    using var gifStream = new GifStream(bytes);
                    while (gifStream.HasMoreData)
                    {
                        switch (gifStream.CurrentToken)
                        {
                            case GifStream.Token.Image:
                                var image = gifStream.ReadImage();
                                var frame = new Texture2D(
                                    gifStream.Header.width,
                                    gifStream.Header.height,
                                    TextureFormat.ARGB32, false);

                                frame.SetPixels32(image.colors);
                                frame.Apply();
                                
                                frames.Add(frame);
                                frameDelays.Add(image.SafeDelaySeconds);
                                break;
                            default:
                                gifStream.SkipToken(); // Other tokens
                                break;
                        }
                    }

                    var gifImage = Instantiate(imagePrefab, contentParent).GetComponent<RawImage>();
                    var width = (float)frames.First().width;
                    var height = (float)frames.First().height;
                    var ratio = width / 375;
                    var le = gifImage.GetComponent<LayoutElement>();
                    le.preferredWidth = width / ratio;
                    le.preferredHeight = height / ratio;
                    _gifs.Add(new Gif { Image = gifImage, Textures = frames, CurrentIndex = 0, Delays = frameDelays });
                }
            }

            bool IsVideo()
            {

                for (var u = 0; u < childNodes.Count; u++)
                {
                    var child = childNodes[u];
                    if (child.Name != "iframe") continue;
                    isVideo = true;

                    var attributes = child.Attributes;
                    for (var t = 0; t < attributes.Count; t++)
                    {
                        var attribute = attributes[t];
                        if (attribute.Name != "src") continue;
                        var video = Instantiate(videoPrefab, contentParent);
                        var videoRect = video.GetComponent<RectTransform>();
                        var onChange = video.GetComponent<OnRectPositionChangedHandler>();
                        var uniObj = new GameObject();
                        var uni = uniObj.AddComponent<UniWebView>();
                        var videoPos = new Vector2(0, videoRect.anchoredPosition.y + 56);
                        uni.ReferenceRectTransform = videoRect;
                        // uni.Frame = new Rect(videoPos, new Vector2(375f, 211f));
                        onChange.OnRectPositionChanged += deltaPosition => {
                            uni.Frame = new Rect(new Vector2(0, -(videoPos.y + deltaPosition)), new Vector2(375f, 211f));
                        };
                        uni.Load(attribute.Value);
                        uni.Show();
                    }
                }
                return isVideo;
            }
            
            void ShowText()
            {
                var text = Instantiate(textPrefab, contentParent).GetComponent<TMP_Text>();
                var newString = paragraph.InnerHtml.Replace("<strong>", "<b>");
                newString = newString.Replace("</strong>", "</b>");
                newString = newString.Replace("<br>", string.Empty);

                var smallHtml = new HtmlDocument();
                smallHtml.LoadHtml(newString);
                for (var index = 0; index < smallHtml.DocumentNode.ChildNodes.Count; index++)
                {
                    var node = smallHtml.DocumentNode.ChildNodes[index];
                    switch (node.Name)
                    {
                        case "b":
                            foreach (var childNode in node.ChildNodes)
                            {
                                if (childNode.Name != "a") continue;
                                ReplaceHrefWithLink(childNode);
                            }

                            continue;
                        case "a":
                        {
                            ReplaceHrefWithLink(node);
                            break;
                        }
                    }
                }

                var tempPath = Path.Combine(Application.persistentDataPath, "temp.txt");
                smallHtml.Save(tempPath);
                newString = File.ReadAllText(tempPath);

                newString = newString.Replace("<a ", "<").Replace("</a>", "</link>");
                var links = newString.Split("link=".ToArray()).ToList();
                newString = links.Aggregate(newString, (current, link) => current.Replace(link + "\">", link + "\">" + "<color=#dd3333>")
                    .Replace("</link>", "</color></link>"));
                text.text = newString;
                File.Delete(tempPath);
            }
        }

        shareButton.onClick.RemoveAllListeners();
        if (!thumbnailFound) return;
        shareButton.onClick.AddListener(() => { Sharing.Share((Texture2D) featuredImage.texture, post.Title.Rendered + "\n\n" + post.Link); });
    }

    private void Update()
    {
        if (!fullPost.enabled) return;
        if (_token.IsCancellationRequested) return;

        _gifCounter += Time.deltaTime;
        if (_gifCounter < 0.05f) return;
        _gifCounter = 0;
        
        for (var i = 0; i < _gifs.Count; i++)
        {
            var gif = _gifs[i];
            var textures = gif.Textures;
            var index = gif.CurrentIndex;
            gif.Image.texture = textures[index++];
            if (index == textures.Count) index = 0;
            gif.CurrentIndex = index;
        }
    }

    public async void ClosePost()
    {
        var tween = _postRect.DOAnchorPosX(375, Duration).SetEase(Ease.Linear);
        while (tween.IsActive()) await Task.Yield();
        fullPost.enabled = false;
        featuredImage.texture = null;
        title.text = string.Empty;
        
        for (var i = contentParent.childCount - 1; i >= 2; i--)
        {
            var obj = contentParent.GetChild(i).gameObject;
            Destroy(obj);
        }
        
        _gifs.Clear();
        _token.Cancel();
    }
}

public class Gif
{
    public RawImage Image;
    public List<Texture> Textures;
    public int CurrentIndex;
    public List<float> Delays;
}