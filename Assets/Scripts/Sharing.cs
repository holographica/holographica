﻿using UnityEngine;

public class Sharing : MonoBehaviour
{
    public static void Share(Texture2D tex, string title)
    {
        var share = new NativeShare();
        share.AddFile(tex);
        share.SetText(title);
        share.Share();
    }
}
